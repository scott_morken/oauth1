<?php

namespace Tests\Smorken\OAuth1\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\OAuth1\Authorization;

class AuthorizationTest extends TestCase
{
    public function testOauthHeader(): void
    {
        $sut = new Authorization;
        $params = [
            'oauth_consumer_key' => 'key',
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_nonce' => '781211bb4b68039bbceacba98d86280b',
            'oauth_timestamp' => '1530049391',
            'oauth_version' => '1.0',
        ];
        $expected = [
            'Authorization' => 'OAuth oauth_consumer_key="key",oauth_signature_method="HMAC-SHA1",oauth_nonce="781211bb4b68039bbceacba98d86280b",oauth_timestamp="1530049391",oauth_version="1.0"',
        ];
        $this->assertEquals($expected, $sut->getHeader($params));
    }
}
