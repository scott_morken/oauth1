<?php

declare(strict_types=1);

namespace Tests\Smorken\OAuth1\Unit;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\OAuth1\Nonce;
use Smorken\OAuth1\Params;

class ParamsTest extends TestCase
{
    #[Test]
    public function it_converts_to_query_string(): void
    {
        $sut = new Params(new Nonce);
        $qs = $sut->toQueryString();
        $this->assertStringStartsWith('oauth_nonce=', $qs);
        $this->assertStringContainsString('&oauth_timestamp=', $qs);
        $this->assertStringEndsWith('&oauth_version=1.0', $qs);
    }
}
