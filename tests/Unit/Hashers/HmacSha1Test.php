<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 9:39 AM
 */

namespace Tests\Smorken\OAuth1\Unit\Hashers;

use PHPUnit\Framework\TestCase;
use Smorken\OAuth1\Hashers\HmacSha1;

class HmacSha1Test extends TestCase
{
    public function testHashWithConsumerSecretAndNoToken(): void
    {
        $h = new HmacSha1('12345');
        $this->assertEquals('X6iRGkSHub4qlMoUymbL507MtmE=', base64_encode($h->hash('foo')));
    }

    public function testHashWithConsumerSecretAndToken(): void
    {
        $h = new HmacSha1('12345', 'abcd');
        $this->assertEquals('+yup/XBnaMePRCMXw90njQXFTos=', base64_encode($h->hash('foo')));
    }

    public function testKeyWithConsumerSecretAndNoToken(): void
    {
        $h = new HmacSha1('12345');
        $this->assertEquals('12345&', $h->getKey());
    }

    public function testKeyWithConsumerSecretAndToken(): void
    {
        $h = new HmacSha1('12345', 'abcd');
        $this->assertEquals('12345&abcd', $h->getKey());
    }
}
