<?php

namespace Tests\Smorken\OAuth1\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\OAuth1\Utils;

class UtilsTest extends TestCase
{
    public function testE(): void
    {
        $test = 'foo%20bar%40baz';
        $this->assertEquals($test, Utils::e($test));
    }
}
