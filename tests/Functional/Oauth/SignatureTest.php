<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/26/18
 * Time: 2:44 PM
 */

namespace Tests\Smorken\OAuth1\Functional\Oauth;

use PHPUnit\Framework\TestCase;
use Smorken\OAuth1\Hashers\HmacSha1;
use Smorken\OAuth1\Hashers\PlainText;
use Smorken\OAuth1\Signature;

class SignatureTest extends TestCase
{
    public function testBaseStringMatchesKnown(): void
    {
        $sut = new Signature(new PlainText('secret'));
        $uri = 'http://term.ie/oauth/example/request_token.php?user_id=abcd&lis_person_name_fname=Fname&lis_person_name_lname=Lname&lis_person_contact_email_primary=email@example.com';
        $params = [
            'oauth_consumer_key' => 'key',
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_nonce' => '322deecc7afa8c79314c475dc01b1a1e',
            'oauth_timestamp' => '1530051357',
            'oauth_version' => '1.0',
        ];
        $sig = $sut->raw($uri, $params, 'GET');
        $this->assertEquals(
            'GET&http%3A%2F%2Fterm.ie%2Foauth%2Fexample%2Frequest_token.php&lis_person_contact_email_primary%3Demail%2540example.com%26lis_person_name_fname%3DFname%26lis_person_name_lname%3DLname%26oauth_consumer_key%3Dkey%26oauth_nonce%3D322deecc7afa8c79314c475dc01b1a1e%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1530051357%26oauth_version%3D1.0%26user_id%3Dabcd',
            $sig
        );
    }

    public function testSignatureMatchesKnown(): void
    {
        $sut = new Signature(new HmacSha1('secret'));
        $uri = 'http://term.ie/oauth/example/request_token.php?user_id=abcd&lis_person_name_fname=Fname&lis_person_name_lname=Lname&lis_person_contact_email_primary=email@example.com';
        $params = [
            'oauth_consumer_key' => 'key',
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_nonce' => '322deecc7afa8c79314c475dc01b1a1e',
            'oauth_timestamp' => '1530051357',
            'oauth_version' => '1.0',
        ];
        $sig = $sut->sign($uri, $params, 'GET');
        $this->assertEquals('g8sCTb6Cbfo5rhAZrvCAmdsmh7o=', $sig);
    }

    public function testSignatureMatchesKnownTwitterGuide(): void
    {
        $uri = 'https://api.twitter.com/1.1/statuses/update.json?include_entities=true';
        $params = [
            'oauth_consumer_key' => 'xvz1evFS4wEEPTGEFPHBog',
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_nonce' => 'kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg',
            'oauth_timestamp' => '1318622958',
            'oauth_token' => '370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb',
            'oauth_version' => '1.0',
            'status' => 'Hello Ladies + Gentlemen, a signed OAuth request!',
            'include_entities' => 'true',
        ];
        $sut = new Signature(new PlainText('secret'));
        $sig = $sut->raw($uri, $params, 'POST');
        $this->assertEquals('POST&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fupdate.json&include_entities%3Dtrue%26oauth_consumer_key%3Dxvz1evFS4wEEPTGEFPHBog%26oauth_nonce%3DkYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1318622958%26oauth_token%3D370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb%26oauth_version%3D1.0%26status%3DHello%2520Ladies%2520%252B%2520Gentlemen%252C%2520a%2520signed%2520OAuth%2520request%2521',
            $sig);
        $sut = new Signature(new HmacSha1('kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw',
            'LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE'));
        $this->assertEquals('hCtSmYh+iHYCEqBWrE7C7hYmtUk=', $sut->sign($uri, $params, 'POST'));
    }

    //    public function testRisan()
    //    {
    //        $uri = 'http://term.ie/oauth/example/request_token.php?user_id=abcd&lis_person_name_fname=Fname&lis_person_name_lname=Lname&lis_person_contact_email_primary=email@example.com';
    //        $params = [
    //            'oauth_consumer_key'               => 'key',
    //            'oauth_signature_method'           => 'HMAC-SHA1',
    //            'oauth_nonce'                      => '322deecc7afa8c79314c475dc01b1a1e',
    //            'oauth_timestamp'                  => '1530051357',
    //            'oauth_version'                    => '1.0',
    //        ];
    //        $sut = new BaseStringBuilder(new UriParser());
    //        $string = $sut->build('GET', $uri, $params);
    //        $this->assertEquals('GET&http%3A%2F%2Fterm.ie%2Foauth%2Fexample%2Frequest_token.php&lis_person_contact_email_primary%3Demail%2540example.com%26lis_person_name_fname%3DFname%26lis_person_name_lname%3DLname%26oauth_consumer_key%3Dkey%26oauth_nonce%3D322deecc7afa8c79314c475dc01b1a1e%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1530051357%26oauth_version%3D1.0%26user_id%3Dabcd', $string);
    //        $sut = new Signature(new PlainText('secret'));
    //        $this->assertSame($string, $sut->raw($uri, $params, 'GET'));
    //        $hmacsut = new HmacSha1Signer();
    //        $signed = $hmacsut->sign($uri, $params, 'GET');
    //        $sut = new Signature(new HmacSha1('secret'));
    //        $this->assertEquals($sut->sign($uri, $params, 'GET'), $signed);
    //    }
}
