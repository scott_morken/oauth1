<?php

namespace Tests\Smorken\OAuth1\Functional\Oauth;

use GuzzleHttp\Psr7\Response;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Smorken\OAuth1\Authorization;
use Smorken\OAuth1\Contracts\Client;
use Smorken\OAuth1\Contracts\Factory;
use Smorken\OAuth1\Hashers\PlainText;
use Smorken\OAuth1\Nonce;
use Smorken\OAuth1\OauthException;
use Smorken\OAuth1\OauthFactory;
use Smorken\OAuth1\Params;
use Smorken\OAuth1\Signature;

class OauthFactoryTest extends TestCase
{
    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testAuthorizeHeaderWithGet(): void
    {
        $sut = $this->getSut();
        $sut->getClient()->shouldReceive('get')
            ->once()
            ->with('localhost?foo=bar', m::type('array'), m::type('array'))
            ->andReturnUsing(function (string $uri, array $auth, array $options) {
                $this->assertEquals('localhost?foo=bar', $uri);
                $this->assertStringStartsWith('OAuth oauth_signature_method="PLAIN-TEXT",oauth_consumer_key="",oauth_nonce="',
                    $auth['Authorization']);
                $contains = [
                    'oauth_version="1.0"',
                    'oauth_timestamp="',
                    'oauth_signature="',
                ];
                foreach ($contains as $contain) {
                    $this->assertStringContainsString($contain, $auth['Authorization']);
                }
                $this->assertEquals([], $options);

                return new Response;
            });
        $this->assertInstanceOf(ResponseInterface::class, $sut->authorizeHeader('localhost', ['foo' => 'bar'], 'GET'));
    }

    public function testAuthorizeHeaderWithPost(): void
    {
        $sut = $this->getSut();
        $sut->getClient()->shouldReceive('post')
            ->once()
            ->with('localhost', m::type('array'), m::type('array'), m::type('array'))
            ->andReturnUsing(function (string $uri, array $auth, array $extra, array $options) {
                $this->assertEquals('localhost', $uri);
                $this->assertStringStartsWith('OAuth oauth_signature_method="PLAIN-TEXT",oauth_consumer_key="",oauth_nonce="',
                    $auth['Authorization']);
                $contains = [
                    'oauth_version="1.0"',
                    'oauth_timestamp="',
                    'oauth_signature="',
                ];
                foreach ($contains as $contain) {
                    $this->assertStringContainsString($contain, $auth['Authorization']);
                }
                $this->assertEquals(['foo' => 'bar'], $extra);
                $this->assertEquals([], $options);

                return new Response;
            });
        $this->assertInstanceOf(ResponseInterface::class, $sut->authorizeHeader('localhost', ['foo' => 'bar']));
    }

    public function testAuthorizeHeaderWithUnexpectedMethodIsException(): void
    {
        $sut = $this->getSut();
        $this->expectException(OauthException::class);
        $this->assertInstanceOf(ResponseInterface::class,
            $sut->authorizeHeader('localhost', ['foo' => 'bar'], 'DELETE'));
    }

    public function testGet(): void
    {
        $sut = $this->getSut();
        $sut->getClient()->shouldReceive('request')
            ->once()
            ->andReturnUsing(function (string $method, string $uri, array $options) {
                $this->assertEquals('GET', $method);
                $contains = [
                    'oauth_signature_method=PLAIN-TEXT',
                    'oauth_nonce=',
                    'oauth_timestamp=',
                    'oauth_version=1.0',
                    'foo=bar',
                    'oauth_signature=',
                ];
                $this->assertStringStartsWith('localhost?', $uri);
                foreach ($contains as $contain) {
                    $this->assertStringContainsString($contain, $uri);
                }
                $this->assertEquals([], $options);

                return new Response;
            });
        $this->assertInstanceOf(ResponseInterface::class, $sut->get('localhost', ['foo' => 'bar']));
    }

    public function testPost(): void
    {
        $sut = $this->getSut();
        $sut->getClient()->shouldReceive('post')
            ->once()
            ->andReturnUsing(function (string $uri, array $headers, array $body, array $options) {
                $contains = [
                    'oauth_signature_method' => 'PLAIN-TEXT',
                    'oauth_nonce' => null,
                    'oauth_timestamp' => null,
                    'oauth_version' => '1.0',
                    'foo' => 'bar',
                    'oauth_signature' => null,
                ];
                $this->assertStringStartsWith('localhost', $uri);
                $this->assertEquals(['Content-Type' => 'application/x-www-form-urlencoded'], $headers);
                foreach ($contains as $key => $value) {
                    $this->assertArrayHasKey($key, $body);
                    if (! is_null($value)) {
                        $this->assertEquals($value, $body[$key]);
                    }
                }
                $this->assertEquals([], $options);

                return new Response;
            });
        $this->assertInstanceOf(ResponseInterface::class, $sut->post('localhost', ['foo' => 'bar']));
    }

    protected function getSut(): Factory
    {
        return new OauthFactory(new Authorization, m::mock(Client::class), new Params(new Nonce),
            new Signature(new PlainText('secret')));
    }
}
