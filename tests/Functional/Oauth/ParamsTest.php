<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/26/18
 * Time: 2:55 PM
 */

namespace Tests\Smorken\OAuth1\Functional\Oauth;

use PHPUnit\Framework\TestCase;
use Smorken\OAuth1\Hashers\HmacSha1;
use Smorken\OAuth1\Nonce;
use Smorken\OAuth1\Params;
use Smorken\OAuth1\Signature;

class ParamsTest extends TestCase
{
    public function testParamsSignatureMatchesKnown(): void
    {
        $uri = 'http://term.ie/oauth/example/request_token.php?user_id=abcd&lis_person_name_fname=Fname&lis_person_name_lname=Lname&lis_person_contact_email_primary=email@example.com';
        $params = [
            'oauth_consumer_key' => 'key',
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_nonce' => '781211bb4b68039bbceacba98d86280b',
            'oauth_timestamp' => '1530049391',
            'oauth_version' => '1.0',
        ];
        $p = (new Params(new Nonce))->newInstance($params);
        $s = new Signature(new HmacSha1('secret'));
        $p->sign($s, $uri, 'GET');
        $this->assertEquals('bhJvQghm2PPIRnRxSpgsgO94VEQ=', $p->oauth_signature);
    }

    public function testParamsSignatureMatchesKnownTwitterGuide(): void
    {
        $uri = 'https://api.twitter.com/1.1/statuses/update.json?include_entities=true';
        $params = [
            'oauth_consumer_key' => 'xvz1evFS4wEEPTGEFPHBog',
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_nonce' => 'kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg',
            'oauth_timestamp' => '1318622958',
            'oauth_token' => '370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb',
            'oauth_version' => '1.0',
            'status' => 'Hello Ladies + Gentlemen, a signed OAuth request!',
        ];
        $p = (new Params(new Nonce))->newInstance($params);
        $s = new Signature(
            new HmacSha1('kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw', 'LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE')
        );
        $p->sign($s, $uri, 'POST');
        $this->assertEquals('hCtSmYh+iHYCEqBWrE7C7hYmtUk=', $p->oauth_signature);
    }

    public function testParamsSignatureMatchesKnownTwitterGuideWithParamInBoth(): void
    {
        $uri = 'https://api.twitter.com/1.1/statuses/update.json?include_entities=true';
        $params = [
            'oauth_consumer_key' => 'xvz1evFS4wEEPTGEFPHBog',
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_nonce' => 'kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg',
            'oauth_timestamp' => '1318622958',
            'oauth_token' => '370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb',
            'oauth_version' => '1.0',
            'status' => 'Hello Ladies + Gentlemen, a signed OAuth request!',
            'include_entities' => 'true',
        ];
        $p = (new Params(new Nonce))->newInstance($params);
        $s = new Signature(
            new HmacSha1('kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw', 'LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE')
        );
        $p->sign($s, $uri, 'POST');
        $this->assertEquals('hCtSmYh+iHYCEqBWrE7C7hYmtUk=', $p->oauth_signature);
    }
}
