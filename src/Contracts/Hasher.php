<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 9:14 AM
 */

namespace Smorken\OAuth1\Contracts;

interface Hasher
{
    public function getConsumerSecret(): string;

    public function getKey(): string;

    public function getMethod(): string;

    public function getTokenSecret(): ?string;

    public function hash(string $string): string;
}
