<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/26/18
 * Time: 8:45 AM
 */

namespace Smorken\OAuth1\Contracts;

interface Params
{
    public function __construct(
        Nonce $nonce
    );

    public function add(array $params = []): void;

    public function getOauthParameters(): array;

    public function getOtherParameters(bool $as_array = true): array|string;

    /**
     * Sets up the base oauth parameters
     */
    public function init(): void;

    public function newInstance(array $params = []): Params;

    public function sign(Signature $signer, string $uri, string $method = 'POST'): Params;

    public function toArray(): array;

    public function toQueryString(): string;
}
