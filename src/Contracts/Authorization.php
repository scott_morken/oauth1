<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/26/18
 * Time: 1:31 PM
 */

namespace Smorken\OAuth1\Contracts;

interface Authorization
{
    public function getHeader(array $params): array;
}
