<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/26/18
 * Time: 9:05 AM
 */

namespace Smorken\OAuth1\Contracts;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface Factory
{
    public function authorizeHeader(
        string $uri,
        array $parameters,
        string $method = 'POST',
        array $options = []
    ): ResponseInterface;

    public function get(string $uri, array $parameters, array $options = []): ResponseInterface;

    public function getClient(): Client;

    public function getParams(): Params;

    public function getSigner(): Signature;

    public function post(string $uri, array $parameters, array $options = []): ResponseInterface;

    public function send(RequestInterface $request, array $options = []): ResponseInterface;
}
