<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 3:38 PM
 */

namespace Smorken\OAuth1\Contracts;

interface Nonce
{
    public function nonce(): string;
}
