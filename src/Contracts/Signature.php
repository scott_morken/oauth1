<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/26/18
 * Time: 8:28 AM
 */

namespace Smorken\OAuth1\Contracts;

interface Signature
{
    /**
     * Get signature method from Hasher
     */
    public function getSignatureMethod(): string;

    /**
     * @return string raw hash
     */
    public function raw(string $uri, array $parameters, string $method = 'POST'): string;

    /**
     * @return string base64 encoded
     */
    public function sign(string $uri, array $parameters, string $method = 'POST'): string;
}
