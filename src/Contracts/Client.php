<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 10:56 AM
 */

namespace Smorken\OAuth1\Contracts;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface Client
{
    public function get(string $uri, array $headers = [], array $options = []): ResponseInterface;

    public function post(string $uri, array $headers = [], array $body = [], array $options = []): ResponseInterface;

    public function request(string $method, string $uri, array $options = []): ResponseInterface;

    public function send(RequestInterface $request, array $options = []): ResponseInterface;
}
