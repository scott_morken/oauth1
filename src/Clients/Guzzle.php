<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 7:33 AM
 */

namespace Smorken\OAuth1\Clients;

use GuzzleHttp\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Smorken\OAuth1\Contracts\Client;

class Guzzle implements Client
{
    public function __construct(protected ClientInterface $client) {}

    public function get($uri, $headers = [], $options = []): ResponseInterface
    {
        $this->addTo('headers', $headers, $options);

        return $this->request('GET', $uri, $options);
    }

    public function post($uri, $headers = [], $body = [], $options = []): ResponseInterface
    {
        $this->addTo('headers', $headers, $options);
        $this->addTo('form_params', $body, $options);

        return $this->request('POST', $uri, $options);
    }

    public function request($method, $uri, $options = []): ResponseInterface
    {
        return $this->client->request($method, $uri, $options);
    }

    public function send(RequestInterface $request, array $options = []): ResponseInterface
    {
        return $this->client->send($request, $options);
    }

    protected function addTo(string $key, array $items, array &$options): void
    {
        if ($items) {
            $options[$key] = $items;
        }
    }
}
