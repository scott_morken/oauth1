<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 7:35 AM
 */

namespace Smorken\OAuth1;

use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\Utils;
use Psr\Http\Message\UriInterface;
use Smorken\OAuth1\Contracts\Hasher;

class Signature implements \Smorken\OAuth1\Contracts\Signature
{
    public function __construct(protected Hasher $hasher) {}

    public function getSignatureMethod(): string
    {
        return $this->hasher->getMethod();
    }

    public function raw(string $uri, array $parameters, string $method = 'POST'): string
    {
        $url = $this->createUrl($uri);
        $baseString = $this->baseString($url, $method, $parameters);

        return $this->hasher->hash($baseString);
    }

    public function sign(string $uri, array $parameters, string $method = 'POST'): string
    {
        return base64_encode($this->raw($uri, $parameters, $method));
    }

    /**
     * Generate a base string for a HMAC-SHA1 signature
     * based on the given url, method, and any parameters.
     */
    protected function baseString(UriInterface $url, string $method = 'POST', array $parameters = []): string
    {
        $baseString = rawurlencode($method).'&';
        $schemeHostPath = Uri::fromParts(
            [
                'scheme' => $url->getScheme(),
                'host' => $url->getHost(),
                'path' => $url->getPath(),
            ]
        );
        $baseString .= rawurlencode($schemeHostPath).'&';
        parse_str($url->getQuery(), $query);
        $data = array_merge($query, $parameters);
        // normalize data key/values
        $data = $this->encodeArray($data);
        ksort($data);
        $baseString .= $this->queryStringFromData($data);

        return $baseString;
    }

    /**
     * Create a Guzzle url for the given URI.
     */
    protected function createUrl(string $uri): UriInterface
    {
        return Utils::uriFor($uri);
    }

    protected function e(string $string): string
    {
        return rawurlencode(rawurldecode($string));
    }

    protected function encodeArray(array $encodable = []): array
    {
        $encoded = [];
        if (is_array($encodable)) {
            foreach ($encodable as $key => $value) {
                $key = \Smorken\OAuth1\Utils::e($key);
                if (is_array($value)) {
                    $encoded[$key] = $this->encodeArray($value);
                } else {
                    $encoded[$key] = \Smorken\OAuth1\Utils::e($value);
                }
            }
        }

        return $encoded;
    }

    /**
     * Creates an array of rawurlencoded strings out of each array key/value pair
     * Handles multi-demensional arrays recursively.
     *
     * @param  array  $data  Array of parameters to convert.
     * @param  array|false  $queryParams  Array to extend. False by default.
     * @param  string  $prevKey  Optional Array key to append
     * @return string rawurlencoded string version of data
     */
    protected function queryStringFromData(array $data, array|false $queryParams = false, string $prevKey = ''): string
    {
        if ($initial = ($queryParams === false)) {
            $queryParams = [];
        }
        foreach ($data as $key => $value) {
            if (strtolower($key) === 'realm' || strtolower($key) === 'oauth_signature') {
                continue;
            }
            if ($prevKey) {
                $key = $prevKey.'['.$key.']'; // Handle multi-dimensional array
            }
            if (is_array($value)) {
                $queryParams = $this->queryStringFromData($value, $queryParams, $key);
            } else {
                //double encoded
                $queryParams[] = rawurlencode($key.'='.$value); // join with equals sign
            }
        }
        if ($initial) {
            return implode('%26', $queryParams); // join with ampersand
        }

        return $queryParams;
    }
}
