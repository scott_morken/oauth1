<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 7:45 AM
 */

namespace Smorken\OAuth1;

class OauthException extends \Exception
{
    protected string $oauth_message;

    public function __construct(
        string $message = '',
        string $oauth_message = '',
        int $code = 0,
        ?\Throwable $previous = null
    ) {
        $this->oauth_message = strip_tags($oauth_message);
        parent::__construct($message, $code, $previous);
    }

    public function getOauthMessage(): string
    {
        return $this->oauth_message;
    }
}
