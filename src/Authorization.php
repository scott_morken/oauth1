<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/26/18
 * Time: 1:31 PM
 */

namespace Smorken\OAuth1;

class Authorization implements \Smorken\OAuth1\Contracts\Authorization
{
    public function getHeader(array $params): array
    {
        $h = $this->parse($params);

        return $this->makeHeader($h);
    }

    protected function makeHeader(array $params): array
    {
        return [
            'Authorization' => sprintf('OAuth %s', implode(',', $params)),
        ];
    }

    protected function parse(array $params): array
    {
        $h = [];
        foreach ($params as $k => $v) {
            $h[] = sprintf('%s="%s"', Utils::e($k), Utils::e($v));
        }

        return $h;
    }
}
