<?php

namespace Smorken\OAuth1;

use Smorken\OAuth1\Contracts\Factory;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application events.
     */
    public function boot(): void
    {
        $this->bootConfig();
    }

    public function register(): void
    {
        $this->app->bind(
            Factory::class,
            function ($c) {
                $secret = $c['config']->get('oauth.consumer_secret', 'secret');
                $signer = new Signature(new Hashers\HmacSha1($secret));

                $params = new Params(new Nonce);

                $client = new Clients\Guzzle(new \GuzzleHttp\Client($this->clientOptions($c)));

                $authorizer = new Authorization;

                return new OauthFactory($authorizer, $client, $params, $signer);
            }
        );
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'oauth');
        // @phpstan-ignore function.notFound
        $this->publishes([$config => config_path('oauth.php')], 'config');
    }

    protected function clientOptions($app): array
    {
        return $app['config']->get('oauth.client_options', []);
    }
}
