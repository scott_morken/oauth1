<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 11:24 AM
 */

namespace Smorken\OAuth1;

class Nonce implements \Smorken\OAuth1\Contracts\Nonce
{
    public function nonce(): string
    {
        return md5((new \DateTime)->getTimestamp().mt_rand(0, 10000));
    }
}
