<?php

namespace Smorken\OAuth1;

class Utils
{
    public static function e(?string $string): string
    {
        return rawurlencode(rawurldecode($string ?? ''));
    }
}
