<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 7:40 AM
 */

namespace Smorken\OAuth1\Hashers;

use Smorken\OAuth1\Contracts\Hasher;

class HmacSha1 extends Base implements Hasher
{
    public function getMethod(): string
    {
        return 'HMAC-SHA1';
    }

    public function hash(string $string): string
    {
        return hash_hmac('sha1', $string, $this->getKey(), true);
    }
}
