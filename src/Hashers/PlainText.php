<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 9:32 AM
 */

namespace Smorken\OAuth1\Hashers;

use Smorken\OAuth1\Contracts\Hasher;

class PlainText extends Base implements Hasher
{
    public function getMethod(): string
    {
        return 'PLAIN-TEXT';
    }

    public function hash(string $string): string
    {
        return $string;
    }
}
