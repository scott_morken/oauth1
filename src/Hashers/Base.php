<?php

namespace Smorken\OAuth1\Hashers;

class Base
{
    protected ?string $token_secret;

    public function __construct(protected string $consumer_secret, ?string $token_secret = null)
    {
        $this->setTokenSecret($token_secret);
    }

    public function getConsumerSecret(): string
    {
        return $this->consumer_secret;
    }

    public function getKey(): string
    {
        return sprintf('%s&%s', $this->e($this->getConsumerSecret()), $this->e($this->getTokenSecret()));
    }

    public function getTokenSecret(): ?string
    {
        return $this->token_secret;
    }

    public function setTokenSecret(?string $token_secret): void
    {
        $this->token_secret = $token_secret;
    }

    protected function e(?string $string): string
    {
        return rawurlencode(rawurldecode($string ?? ''));
    }
}
