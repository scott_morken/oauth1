<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 8:22 AM
 */

namespace Smorken\OAuth1;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Smorken\OAuth1\Contracts\Client;
use Smorken\OAuth1\Contracts\Factory;

class OauthFactory implements Factory
{
    public function __construct(
        protected \Smorken\OAuth1\Contracts\Authorization $authorization,
        protected Client $client,
        protected \Smorken\OAuth1\Contracts\Params $params,
        protected \Smorken\OAuth1\Contracts\Signature $signer
    ) {}

    public function authorizeHeader(
        string $uri,
        array $parameters,
        string $method = 'POST',
        array $options = []
    ): ResponseInterface {
        $method = strtoupper($method);
        $params = $this->getParams()
            ->newInstance($parameters);
        $params->sign($this->getSigner(), $uri, $method);
        $headers = $this->getAuthorization()
            ->getHeader($params->getOauthParameters());
        if ($method === 'POST') {
            return $this->getClient()
                ->post($uri, $headers, $params->getOtherParameters(), $options);
        }
        if ($method === 'GET') {
            $uri = $this->mergeQueryString($uri, $params->getOtherParameters());

            return $this->getClient()
                ->get($uri, $headers, $options);
        }
        throw new OauthException("Method $method not allowed.");
    }

    public function get(string $uri, array $parameters, array $options = []): ResponseInterface
    {
        $params = $this->getParams()
            ->newInstance($parameters);
        $uri = $this->mergeQueryString(
            $uri,
            $params->sign($this->getSigner(), $uri, 'GET')
                ->toArray()
        );

        return $this->getClient()
            ->request('GET', $uri, $options);
    }

    public function getAuthorization(): \Smorken\OAuth1\Contracts\Authorization
    {
        return $this->authorization;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getParams(): \Smorken\OAuth1\Contracts\Params
    {
        return $this->params;
    }

    public function getSigner(): \Smorken\OAuth1\Contracts\Signature
    {
        return $this->signer;
    }

    public function post(string $uri, array $parameters, array $options = []): ResponseInterface
    {
        $params = $this->getParams()
            ->newInstance($parameters);

        return $this->getClient()
            ->post(
                $uri,
                ['Content-Type' => 'application/x-www-form-urlencoded'],
                $params->sign($this->getSigner(), $uri, 'POST')
                    ->toArray(),
                $options
            );
    }

    public function send(RequestInterface $request, array $options = []): ResponseInterface
    {
        return $this->getClient()
            ->send($request, $options);
    }

    protected function mergeQueryString(string $uri, array $params): string
    {
        $uri_qs_str = parse_url($uri, PHP_URL_QUERY);
        parse_str($uri_qs_str ?? '', $uri_qs);
        foreach ($params as $k => $v) {
            $uri_qs[$k] = $v;
        }
        if ($uri_qs) {
            $uri_parts = explode('?', $uri);

            return sprintf('%s?%s', $uri_parts[0], http_build_query($uri_qs));
        }

        return $uri;
    }
}
