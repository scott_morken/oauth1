<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/25/18
 * Time: 11:29 AM
 */

namespace Smorken\OAuth1;

class Params implements \Smorken\OAuth1\Contracts\Params
{
    protected array $params = [
        'oauth_signature_method' => null,
        'oauth_consumer_key' => null,
        'oauth_nonce' => null,
        'oauth_timestamp' => null,
        'oauth_version' => null,
    ];

    public function __construct(
        protected \Smorken\OAuth1\Contracts\Nonce $nonce
    ) {
        $this->init();
    }

    public function __get(string $key): ?string
    {
        return $this->params[$key] ?? null;
    }

    public function __set(string $key, ?string $value): void
    {
        $this->params[$key] = $value;
    }

    public function add(array $params = []): void
    {
        foreach ($params as $k => $v) {
            $this->params[trim($k)] = trim((string) $v);
        }
    }

    public function getOauthParameters(): array
    {
        return $this->split($this->params);
    }

    public function getOtherParameters(bool $as_array = true): string|array
    {
        $params = $this->split($this->params, false);
        if ($as_array) {
            return $params;
        }

        return $this->convertToQueryString($params);
    }

    public function init(): void
    {
        $this->params['oauth_nonce'] = $this->nonce->nonce();
        $this->params['oauth_timestamp'] = $this->timestamp();
        $this->params['oauth_version'] = $this->getVersion();
    }

    public function newInstance(array $params = []): static
    {
        $p = new static($this->nonce);
        $p->add($params);

        return $p;
    }

    /**
     * @return $this
     */
    public function sign(
        \Smorken\OAuth1\Contracts\Signature $signer,
        string $uri,
        string $method = 'POST'
    ): \Smorken\OAuth1\Contracts\Params {
        $this->params['oauth_signature_method'] = $signer->getSignatureMethod();
        $this->params['oauth_signature'] = $signer->sign($uri, $this->params, $method);

        return $this;
    }

    public function toArray(): array
    {
        return $this->params;
    }

    public function toQueryString(): string
    {
        return $this->convertToQueryString($this->params);
    }

    protected function convertToQueryString(array $params): string
    {
        return http_build_query($params, '', '&');
    }

    protected function getVersion(): string
    {
        return '1.0';
    }

    protected function split(array $params, bool $oauth = true): array
    {
        $h = [];
        foreach ($params as $k => $v) {
            $lk = strtolower($k);
            $is_oauth = ($lk === 'realm' || str_starts_with($lk, 'oauth_'));
            if ($oauth && $is_oauth) {
                $h[$k] = $v;
            } elseif (! $oauth && ! $is_oauth) {
                $h[$k] = $v;
            }
        }

        return $h;
    }

    protected function timestamp(): int
    {
        return (new \DateTime)->getTimestamp();
    }
}
