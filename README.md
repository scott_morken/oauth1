### OAuth1 package

This library is built specifically for use with Brainfuse.  It may not have all of
the functionality necessary for a complete OAuth service call. But it might if you
are creative!

#### Laravel

* Adding to your composer.json should auto-register the service provider, if not:

    * Add `\Smorken\OAuth1\ServiceProvider` to the `config/app.php` service providers

* If needed, publish the config
    * `php artisan vendor:publish --provider="Smorken\OAuth1\ServiceProvider" --tag=config`

* Environment variables are available for most options in the config (.env)
    
* Factory
    * `$factory = app(\Smorken\OAuth1\Contracts\Factory::class);`

#### Use

The whole process is as follows (Request Token):
```php
$uri = 'http://term.ie/oauth/example/request_token.php';

$opts = [
    'user_id'                          => 'abcd',
    'lis_person_name_fname'            => 'Fname',
    'lis_person_name_lname'            => 'Lname',
    'lis_person_contact_email_primary' => 'email@example.com',
    'oauth_consumer_key'               => 'key,
];

$signer = new Signature(new \Smorken\OAuth1\Hashers\HmacSha1('secret'));

$params = new Params(new Nonce());

$client = new \Smorken\OAuth1\Clients\Guzzle(new \GuzzleHttp\Client());

$factory = new OauthFactory(new Authorization(), $client, $params, $signer);

$resp = $factory->post($uri, $opts, ['debug' => true]);
// OR
//$resp = $factory->get($uri, $opts);
// OR
//$resp = $factory->authorizationHeader($uri, $opts, 'POST');

echo $resp->getBody() . PHP_EOL;
```

You can also manually follow the steps in the Factory if the defaults don't 
do what you need.

Example of building a request manually
```php
$uri = 'http://term.ie/oauth/example/request_token.php';

$opts = [
    'user_id'                          => 'abcd',
    'lis_person_name_fname'            => 'Fname',
    'lis_person_name_lname'            => 'Lname',
    'lis_person_contact_email_primary' => 'email@example.com',
    'oauth_consumer_key'               => 'key,
];

$signer = new Signature(new \Smorken\OAuth1\Hashers\HmacSha1('secret'));

$params = new Params(new Nonce());

$client = new \Smorken\OAuth1\Clients\Guzzle(new \GuzzleHttp\Client());

$request = new \GuzzleHttp\Psr7\Request(
    'POST',
    $uri,
    ['Content-Type' => 'application/x-www-form-urlencoded'],
    $params->sign($signer, $uri, 'POST')->toQueryString()
);

$resp = $client->send($request);

echo $resp->getBody() . PHP_EOL;
```
