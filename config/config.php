<?php

return [
    'consumer_secret' => env('OAUTH_CONSUMER_SECRET'),
    'consumer_key' => env('OAUTH_CONSUMER_KEY'),
    'urls' => [
        'request_token' => env('OAUTH_REQUEST_TOKEN_URL'),
        'authorization' => env('OAUTH_AUTHORIZATION_URL'),
        'access_token' => env('OAUTH_ACCESS_TOKEN_URL'),
    ],
    'client_options' => [
        'timeout' => 10,
    ],
];
